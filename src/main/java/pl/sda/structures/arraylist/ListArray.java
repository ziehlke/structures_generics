package pl.sda.structures.arraylist;

public class ListArray<T> {
    private T[] array;
    private int size;

    public ListArray() {
        this.array = (T[]) new Object[5];
        this.size = 0;
    }

    public void add(T elementToAdd) {
        // 1. sprawdzam rozmiar tablicy i porównuję z ilością elementów (this.size)
        if (this.size >= this.array.length) {
            // jeśli brakuje miejsca to wywołuję metodę, która przepisze mi listę
            // i doda miejsca w tablicy
            extendArray();
        }
        // wstawiam nowy element na koniec.
        this.array[this.size] = elementToAdd;

        // zwiększam ilość elementów
        this.size++;
    }

    /**
     * Usuwa z listy element na podanej pozycji.
     *
     * @param position - indeks elementu do usunięcia.
     */
    public void remove(int position) {

        // w pętli iterujemy od elementu z pozycji usuwanej
        // do ostatniego elementu (size - 1)
        // przestawiamy element z pozycji i+1 na pozycję i

        for (int i = position; i < this.size - 1; i++) {
            this.array[i] = this.array[i + 1];
        }

        this.size--;
    }

    public void add(int position, T elementToAdd) {
        // sprawdzenie rozmiaru i jeśli zaistnieje potrzeba rozszerzenie tablicy
        if (this.size >= this.array.length) {
            extendArray();
        }

        // przepisuję elementy (od ostatniego do miejsca w które
        // chcę wstawić nowy element
        for (int i = this.size - 1; i >= position; i--) {
            this.array[i + 1] = this.array[i];
        }

        // wstawienie elementu na pozycję 'position'
        this.array[position] = elementToAdd;
        // zwiększenie rozmiaru
        this.size++;
    }

    public T get(int indeks) {
        return this.array[indeks];
    }

    private void extendArray() {
        // tworzę dwukrotnie większą tablicę (T[])new Object[size];
        T[] arrayCopy = (T[]) new Object[this.array.length * 2];

        // przepisuję wszystkie elementy z orginalnej tablicy do kopii
        for (int i = 0; i < this.array.length; i++) {
            arrayCopy[i] = this.array[i];
        }

        // zastępuję oryginał kopią
        this.array = arrayCopy;
    }

    @Override
    public String toString() {
        return "ListArray{" +
                "size=" + size +
                "elements=[" + printElements() + "]" +
                '}';
    }

    private String printElements() {
        // pusty string do wypisania
        String elements = "";

        // dla każdego elementu dopisuję go i przecinek
        for (int i = 0; i < this.size; i++) {
            elements += this.array[i] + ", ";
        }

        // zwracam ciąg z elementami
        return elements;
    }

    public int size() {
        return this.size;
    }

    public boolean contains(T searchedObject) {
        int elementIndex = indexOf(searchedObject);

        // jeśli jest indeks, to znaczy że element istnieje (contains = true)
        if (elementIndex != -1) {
            return true;
        }

        // jeśli elementIndex == -1 to znaczy że nie odnaleźliśmy elementu.
        return false;
    }

    public int indexOf(T searchedElement) {
        // iterujemy całą tablicę w pętli
        for (int i = 0; i < this.size; i++) {
            // jeśli element na pozycji i to element szukany
            if (this.array[i] == searchedElement) {
                // zwracamy jego indeks
                return i;
            }
        }

        // zwracamy -1 - element nie został odnaleziony
        return -1;
    }

    public void clear() {
        // czyszczenie to po prostu zmniejszenie rozmiaru (ilości elementów)
        // do zera. Nie ma potrzeby usuwania elementów z tablicy i ustawiania ich
        // na null, bo po dodaniu nowych elementów będą one nadpisywały stare.
        this.size = 0;
    }

    public void removeLast() {
        // zmniejszam ilość elementów. opcjonalnie można element na ostatniej pozycji
        // ustawić na null, ale nie ma takiej potrzeby, bo dodanie nowego elementu na
        // koniec spowoduje nadpisanie ostatniego elementu.
        this.size--;
    }
}
