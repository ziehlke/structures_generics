package pl.sda.structures;


import pl.sda.structures.arraylist.ListArray;
import pl.sda.structures.linkedlist.ListLinked;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // arraylist
//        ListArray listArray = new ListArray();
//        listArray.add(1);
//        listArray.add(2);
//        listArray.add(3);
//        listArray.add(5);
//        listArray.add(10);
//        listArray.add(15);
//        listArray.add(20);
//
//        listArray.add(1, 100);
//        listArray.add(5, 500);
//        listArray.add(8, 800);
//
//        System.out.println(listArray);
//        listArray.remove(1);
//        System.out.println(listArray);

        // size - zwraca rozmiar listy
        // contains - sprawdza czy element znajduje się w liście
        // indexOf - zwraca indeks elementu podanego w parametrze
        // set - przyjmuje indeks i obiekt i ustawia na pozycji indeks podany obiekt
        // clear - czyści listę

/*        ListLinked listLinked = new ListLinked();
        listLinked.add(1);
        listLinked.add(2);
        listLinked.add(3);
        listLinked.add(4);
        listLinked.add(5);
        listLinked.add(6);
        listLinked.add(10);

        System.out.println(listLinked);

        listLinked.add(1, 100);
        System.out.println(listLinked);
        listLinked.add(5, 500);
        System.out.println(listLinked);

        listLinked.add(0, 1000);
        System.out.println(listLinked);
        listLinked.add(10, 10000);
        System.out.println(listLinked);

        listLinked.remove(0);
        System.out.println(listLinked);
        listLinked.add(0, 30);
        System.out.println(listLinked);
        System.out.println(listLinked.indexOf(100));

        listLinked.remove(listLinked.size()-1);
        System.out.println(listLinked);
        listLinked.remove(listLinked.size()-1);
        System.out.println(listLinked);
//        listLinked.remove(0);

        */

        ListLinked<Integer> listLinked = new ListLinked<>();
        listLinked.add(5);
//        listLinked.add("5");

        ListLinked<String> listLinked1 = new ListLinked<>();
        listLinked1.add("sd");
//        listLinked1.add(5);


        ListArray<Integer> integerListArray = new ListArray<>();
        integerListArray.add(5);
//        integerListArray.add("string");
        
        ListArray<String> stringListArray = new ListArray<>();
//        stringListArray.add(5);
        stringListArray.add("pięć");

    }
}
