package pl.sda.structures;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BenchmarkWrongWay {
    public static void main(String[] args) {

        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(i);
        }

//        Long startTime = System.currentTimeMillis();
//        for (int i = 0; i < list.size(); i++) { // n
//            list.get(i); // n
//        }
//
//        System.out.println(System.currentTimeMillis() - startTime);


        Long startTime = System.currentTimeMillis();
        for (Integer i : list) { // n
            i++;
        }

        System.out.println(System.currentTimeMillis() - startTime);
    }
}
