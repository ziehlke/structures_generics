package pl.sda.structures.linkedlist;

public class ListLinked<T> {
    private Node head; // pierwszy element listy
    private Node tail; // ostatni element listy

    private int size;
    private String elementsString;

    public ListLinked() {
        this.head = null;
        this.tail = null;

        this.size = 0;
    }

    public void add(T elementToAdd) {
        Node node = new Node(elementToAdd);

        // sprawdzam czy lista jest pusta
        if (this.head == null && this.tail == null) {
            this.head = node;
            this.tail = node;
            // nowy węzeł jest nowym początkiem i końcem listy
        } else if (this.head == this.tail && this.size == 1) {
            // to znaczy że jest jeden element
            this.tail = node;
            this.head.setNext(node);

            this.tail.setPrev(this.head);
        } else {
            node.setPrev(this.tail);
            this.tail.setNext(node);

            this.tail = node;
        }

        this.size++;
    }

    public void removeLast() {
        if (this.tail != null) {
            // od teraz ostatnim elementem jest element przedostatni
            // (this.tail.getPrev()) - poprzednik ostatniego, czyli przedostatni
            this.tail = this.tail.getPrev();
            this.tail.setNext(null);

            // zmiana rozmiaru listy (-1)
            this.size--;
        }
    }

    public void removeFirst() {
        // operacja możliwa tylko gdy mamy jakikolwiek pierwszy element
        if (this.head != null) {
            // od teraz pierwszym elementem jest drugi element
            this.head = this.head.getNext();
            this.head.setPrev(null);

            this.size--;
        }
    }

    @Override
    public String toString() {
        return "ListLinked{" + getElementsString() + "}";
    }

    public String getElementsString() {

        String elementsString = "";

        Node tmp = this.head;
        while (tmp != this.tail) {
            elementsString += tmp.getData() + ", ";
            tmp = tmp.getNext();
        }
        if (tmp != null) {
            elementsString += tmp.getData();
        }
        return elementsString;
    }

    public void remove(int index) {
        Node tmp = this.head;

        // licznik skoków - ile razy przeszliśmy do następnego węzła
        int jumpCount = 0;
        while (tmp != null && jumpCount != index) {
            tmp = tmp.getNext();
            jumpCount++;
        }

        if (tmp == this.head) {
            this.head = tmp.getNext();
        } else {
            // gdy nie usuwamy head
            tmp.getPrev().setNext(tmp.getNext());
        }

        if (tmp == this.tail) {
            this.tail = tmp.getPrev();
        } else {
            tmp.getNext().setPrev(tmp.getPrev());
        }

        this.size--;
    }

    public void add(int index, T elementToAdd) {
        Node node = new Node(elementToAdd);

        Node tmp = this.head;

        // licznik skoków - ile razy przeszliśmy do następnego węzła
        int jumpCount = 0;
        while (tmp != null && jumpCount != index) {
            tmp = tmp.getNext();
            jumpCount++;
        }
        // wstawiamy na ostatnią pozycję
        if (tmp == null && jumpCount == index) {
            add(elementToAdd);
            return;
        } else if (tmp == null && jumpCount != index) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        node.setPrev(tmp.getPrev());
        node.setNext(tmp);

        // kiedy wstawiamy w indeks 0 nie możemy sięgnąć poprzednika (bo go nie ma)
        if (tmp.getPrev() != null) {
            // indeks 0 nie posiada poprzednika!
            tmp.getPrev().setNext(node);
        } else {
            this.head = node; // kiedy wstawiamy na indeks 0
        }
        tmp.setPrev(node);

        this.size++;
    }

    public int size() {
        return this.size;
    }

    public int sizeWithTmp() {
        Node tmp = this.head;

        // licznik skoków - ile razy przeszliśmy do następnego węzła
        int jumpCount = 0;
        while (tmp != null) {
            tmp = tmp.getNext();
            jumpCount++;
        }

        return jumpCount;
    }

    public int indexOf(T searchedObject) {
        Node tmp = this.head;

        // licznik skoków - ile razy przeszliśmy do następnego węzła
        int jumpCount = 0;
        while (tmp != null) {
            if (tmp.getData() == searchedObject) {
                return jumpCount;
            }
            tmp = tmp.getNext();
            jumpCount++;
        }

        return -1;
    }

    public boolean contains(T searchedObject) {
        return indexOf(searchedObject) != -1;
    }

    // int size(); - zwróć wartość pola size
    // int sizeWithTmp(); - przechodząc po elementach
    // boolean contains(Object szukany);
    // int indexOf(Object szukany);
}
